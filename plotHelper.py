
def binningReorder(binning):
    binnings = binning.split(",")
    binningReordered = "%s,%s,%s,%s,%s,%s"%(binnings[3], binnings[4], binnings[5],
                                            binnings[0], binnings[1], binnings[2])
    return binningReordered


varNameDic={
    "cellTime" : ["cellTime", "Cell time [ns]"],
    "cellE" : ["cellE", "Reco cell energy [GeV]"],
    "cellEdep" : ["cellEdep", "Truth cell energy [GeV]"],
    "cellEta" : ["cellEta", "Cell #eta"],
    "cellPhi" : ["cellPhi", "Cell #phi"],
    "cellCenterX" : ["cellCenterX", "Cell center X"],
    "cellCenterY" : ["cellCenterY", "Cell center Y"],
    "cellCenterZ" : ["cellCenterZ", "Cell center Z"],
    "cellCenterR" : ["TMath::Sqrt(cellCenterX*cellCenterX+cellCenterY*cellCenterY)", "Cell center R"],
    "truthE" : ["truthE", "Truth Pion Energy [GeV]"],
    "truthEta" : ["truthEta", "Truth Pion #eta"],
    "truthPt" : ["truthPt", "Truth Pion P_{T} [GeV]"],
    "truthPhi" : ["truthPhi", "Truth Pion #phi"],
    "logtruthE" : ["log10(truthE)", "Log10(Truth Pion Energy [GeV])"],
    "nCluster" : ["nCluster", "Number of clusters in a pion"],
    "cluster_CELL_SIGNIFICANCE" : ["cluster_CELL_SIGNIFICANCE", "cluster CELL #sigma"],
    "presampler" : ["TMath::Sqrt(cellCenterX*cellCenterX+cellCenterY*cellCenterY)<1500", "cluster CELL #sigma"],
    "layer1" : ["TMath::Sqrt(cellCenterX*cellCenterX+cellCenterY*cellCenterY)>1500&&TMath::Sqrt(cellCenterX*cellCenterX+cellCenterY*cellCenterY)<1600", "cluster CELL #sigma"],
    "layer2" : ["TMath::Sqrt(cellCenterX*cellCenterX+cellCenterY*cellCenterY)>1600&&TMath::Sqrt(cellCenterX*cellCenterX+cellCenterY*cellCenterY)<1800", "cluster CELL #sigma"],
    "layer3" : ["TMath::Sqrt(cellCenterX*cellCenterX+cellCenterY*cellCenterY)>1800&&TMath::Sqrt(cellCenterX*cellCenterX+cellCenterY*cellCenterY)<2100", "cluster CELL #sigma"],
}

def addcutReplacer(addcut):
    newaddcut = addcut
    for var in varNameDic.keys():
        if var in addcut: newaddcut = newaddcut.replace(var, varNameDic[var][0])

    return newaddcut

def varNameConverter(var):
    title = []
    varStrList = var.split(":") 

    if len(varStrList)>1: varStr = "%s:%s"%(varStrList[1], varStrList[0]) 
    else: varStr = varStrList[0]

    for varName in varStrList: #[::-1]:
        if varName in var: 
            varStr = varStr.replace(varName, varNameDic[varName][0])
            title.append(varNameDic[varName][1])
            
    return varStr, title
