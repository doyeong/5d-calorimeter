import ROOT
from ROOT import gROOT
gROOT.SetBatch(ROOT.kTRUE)
import sys 
import argparse
import plotHelper


parser = argparse.ArgumentParser()
parser.add_argument('--var', '-v', help='variable to draw', action='store', type=str, default = "cellTime")
parser.add_argument('--binning', '-b', help='binning,min,max', action='store', type=str, default = "100,-50,50")
parser.add_argument('--addcut', '-a', help='additional cut as a string foam', action='store', type=str, default = "1")
parser.add_argument('--logy', '-l', action="store_true", default=False, help='logy if it is added')
parser.add_argument('--doFit', '-f', action="store_true", default=False, help='Gaussian fit')
parser.add_argument('--doFitRange', '-r', help='min,max', action='store', type=str, default = "-5,5")
parser.add_argument('--sample', '-s', help='pizero,piminus,pizero.PU,piminus.PU', action='store', type=str, default = "piminus")
args = parser.parse_args()

def addTEXT(text, lowY, color, textsize):
    lowX=0.17
    #lowY=0.7
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    if "pi" not in text: lumi.SetTextFont(61)
    lumi.SetTextSize(textsize)
    lumi.SetBorderSize(    0 )
    lumi.SetFillStyle(     0 )
    lumi.SetTextAlign(    12 )
    lumi.SetTextColor( color )
    lumi.AddText(text)
    return lumi


## define main function
def main():
    # Open file & get TTree
    InputFile=ROOT.TFile("../ntuples/%s.topo-cluster.001.new.root"%(args.sample))
    #InputFile=ROOT.TFile("/pnfs/usatlas.bnl.gov/users/dkim3/piminus.PU.topo-cluster.001.origin.root")
    #InputFile=ROOT.TFile("/pnfs/usatlas.bnl.gov/users/dkim3/piminus.topo-cluster.001.new.root")
    tree = InputFile.Get("CellTree")
    clusterTree = InputFile.Get("ClusterTree")

    var, title = plotHelper.varNameConverter(args.var)
    drawStr = "%s>>h(%s)"%(var, args.binning)
    addcut = plotHelper.addcutReplacer(args.addcut)
    tree.Draw(drawStr, addcut) 

    # DRAW
    canvas = ROOT.TCanvas("","",0,0,700,700)
    canvas.cd()
    InputFile.cd()
    h=InputFile.Get("h")
    h.SetTitle("") if addcut=="1"  else h.SetTitle(addcut)    
    if "cellTime<" in addcut:
        #firstTitle = args.addcut[:(args.addcut).find("&& ")]
        #secondTitle = args.addcut[(args.addcut).find("&& ")+3:]
        firstTitle = addcut[:(addcut).find("cellTime>")-4]
        secondTitle = addcut[(addcut).find("cellTime>"):]

        if "cellTime>=" in addcut:
            low = secondTitle[secondTitle.find(">=")+2:secondTitle.find("&&")]
            lowineq = "<="
        elif "cellTime>" in addcut:
            low = secondTitle[secondTitle.find(">")+1:secondTitle.find("&&")]
            lowineq = "<"
        if "cellTime<" in addcut:
            high = secondTitle[secondTitle.find("<")+1:] #secondTitle.rfind("&&")]
            #h.SetMaximum(130)
        secondTitle = "%s (ns) %s cellTime < %s (ns)"%(low, lowineq, high)
        h.SetTitle("#splitline{%s}{%s}"%(firstTitle, secondTitle))

    if len(title)==2:
        ROOT.gStyle.SetOptStat(0)
        canvas.SetLeftMargin(0.11)
        canvas.SetRightMargin(0.14)
    else:
        canvas.SetLeftMargin(0.14)
        canvas.SetRightMargin(0.11)
    canvas.SetBottomMargin(0.1)        
    canvas.SetTopMargin(0.15)
    #h.SetMaximum(10)
    h.GetXaxis().SetTitle(title[0])
    h.GetYaxis().SetTitle("evt / bin") if len(title)<2 else h.GetYaxis().SetTitle(title[1])

    if len(title)==1: h.Draw("HISTE")          
    elif len(title)==2: h.Draw("COLZ")  

    

    if args.doFit is True:
        fittype = "gaus"
        doFitRange = args.doFitRange.split(",")
        fitFx = h.Fit(fittype, "", "", float(doFitRange[0]), float(doFitRange[1]))
        fx = h.GetFunction(fittype)
        gausMean = fx.GetParameter(1)
        gausSigma = fx.GetParameter(2)
        fx.Draw("SAME")
        mean = addTEXT("Mean = %.2f ns"%(gausMean), 0.68, 2, 0.04)
        sigma = addTEXT("RMS = %.f ps"%(gausSigma*1000), 0.63, 2, 0.04)
        mean.Draw("SAME")
        sigma.Draw("SAME")

        h_mean = addTEXT("Mean = %.2f ns"%(h.GetMean()), 0.55, 4, 0.04)
        h_rms = addTEXT("RMS = %.f ps"%(h.GetRMS()*1000), 0.5, 4, 0.04)
        h_mean.Draw("SAME")
        h_rms.Draw("SAME")

    sampleType_str = "#pi^{0}" if "zero" in args.sample else "#pi^{-}"
    if "PU" in args.sample:    sampleType_str = sampleType_str + " PU" 
    sampleType = addTEXT(sampleType_str, 0.05, 1, 0.07)
    sampleType.Draw("SAME")

    print ("#entries: %.f"%(h.GetEntries()))
    if args.logy==True: canvas.SetLogy()

    # Linear line
    if "cellE" in var and "cellEdep" in var and ":" in var and "cellEta" not in var:
        binning = args.binning.split(",")

        l = ROOT.TLine(float(binning[1]), float(binning[4]), float(binning[2]), float(binning[5]))
        l.SetLineColor(2)
        l.SetLineWidth(2)
        l.Draw("SAME")

        if float(binning[1]) < 0: # dashed lines for E=0
            lh = ROOT.TLine(float(binning[1]), 0, float(binning[2]), 0)
            lh.SetLineColor(16)
            lh.SetLineWidth(2)
            lh.SetLineStyle(7)
            lh.Draw("SAME")

        if float(binning[4]) < 0: # dashed lines for E=0
            lv = ROOT.TLine(0, float(binning[4]), 0, float(binning[5]))
            lv.SetLineColor(16)
            lv.SetLineWidth(2)
            lv.SetLineStyle(7)
            lv.Draw("SAME")

        
    # extract meta info
    pngname = "plots/%s.png"%((args.var).replace("/","_"))
    canvas.SaveAs(pngname)
    canvas.SaveAs(pngname.replace("png", "pdf"))

    # Open file to store histogram
    filenameOut = "%s.root"%((args.var).replace("/","_"))
    fOut = ROOT.TFile(filenameOut, 'RECREATE')
    fOut.cd()
    h.Clone().Write()
    fOut.Close()

    
if __name__ == "__main__":
    main()





# 806
