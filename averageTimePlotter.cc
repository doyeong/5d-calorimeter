#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <TROOT.h>
#include <TStyle.h>
#include <TFile.h>
#include <TTree.h>
#include <TH1.h>
#include <TPaveText.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <tuple>
#include <map>

struct Entry {
  Int_t eventNumber;
  Int_t nCellsWithCuts;
  Float_t cellTime;
  Float_t cellEdep;
  Float_t cellE;
  Float_t cellEinPion;
  Float_t cellEta;
  Float_t cellCenterX;
  Float_t cellCenterY;
  Float_t truthE;
  Float_t truthPt;
};

// Create a map with string keys and integer values
std::map<std::tuple<std::string, float, float>, float> myRMSMap;  // first tuple = layer, Emin, Emax boundaries, 2nd vector is rms
std::map<std::tuple<std::string, float, float>, float> myMEANMap;  // first tuple = layer, Emin, Emax boundaries, 2nd vector is mean
void ReadTxt() {
  std::ifstream file("MeanRMSMapper.txt"); // Open the file for reading
  if (!file.is_open()) {
    std::cerr << "Failed to open the file." << std::endl;
    return 1;
  }

  std::string line;

  while (std::getline(file, line)) {
    // Create an input string stream
    std::istringstream iss(line);
    // Vector to store the split tokens
    std::vector<std::string> tokens;
    // Split the string using space as the delimiter
    std::string token;
    while (iss >> token) {
      tokens.push_back(token);
    }
    std::tuple<std::string, float, float> tmptuple = std::make_tuple(tokens[0], std::stof(tokens[1]), std::stof(tokens[2]));
    myRMSMap[std::make_tuple(tokens[0], std::stof(tokens[1]), std::stof(tokens[2]))] = std::stof(tokens[6]); // index 5 = histo mean, 6 = histo rms, 3 = fit mean, 4 = fit rms
    myMEANMap[std::make_tuple(tokens[0], std::stof(tokens[1]), std::stof(tokens[2]))] = std::stof(tokens[5]); // index 5 = histo mean, 6 = histo rms, 3 = fit mean, 4 = fit rms
  }
  file.close(); // Close the file
}


typedef std::tuple<Float_t, Float_t, Float_t, std::string> CellInfoTuple; // Change this to the actual type if needed

float lookupRMSmap(float cellE, std::string layer) {
  float a, b, c;
  if (layer=="presampler") {
    a = 0.307004;
    b = -0.000110;
    c = 0.000041;
  } else if (layer=="layer1") {
    a = 0.058408;
    b = 0.002349;
    c = 0.023403;
  } else if (layer=="layer2") {
    a = 0.043240;
    b = 0.092432;
    c = 0.107603;
  } else if (layer=="layer3") {
    a = 0.054027;
    b = 0.125659;
    c = 0.065723;
  }
  return TMath::Sqrt(a*a + (b*b/cellE) + (c*c/(cellE*cellE)));
}

float lookupMEANmap(float cellE, std::string layer) {
  float a, b, c, d;
  if (layer=="presampler") { // FIXME
    a = 453.415703;
    b = 0.000201;
    c = -453.286200;
  } else if (layer=="layer1") {
    a = 0.046005;
    b = 4.273973;
    c = 0.013775;
  } else if (layer=="layer2") {
    a = 0.072752;
    b = 1.797878;
    c = 0.019124;
  } else if (layer=="layer3") {
    a = 0.045590;
    b = 4.019339;
    c = 0.019019;
  }
  return a*(TMath::Exp(-cellE/b)) + c;
}

void drawHist(TH1D* h1, const std::vector<Float_t>& data) {
  for (const auto& x : data) {
    h1->Fill(x);
  }
}

// h_frac will store histgram of cellE/truthE that used in the gaussian distribution
std::vector<Float_t> vec_frac;
TH1D* h_frac = new TH1D("h_fraction", "", 100, 0, 1);
// h_ncell will store histgram of #cells in the gaussian distribution from each pion
std::vector<Float_t> vec_ncell;
TH1D* h_ncell = new TH1D("h_ncell", "", 50, 0, 50);

Float_t sortmaxValueInVector(std::vector<CellInfoTuple> cellInfoAPion, int howmany) {
  // Sort the vector based on the second element of each tuple in descending order
  std::sort(cellInfoAPion.begin(), cellInfoAPion.end(),
	    [](const CellInfoTuple& a, const CellInfoTuple& b) {
	      return std::get<1>(a) > std::get<1>(b);
	    });

  // Calculate the sum of the first elements from 0th to nth elements
  Float_t sum = 0.0f; 
  Float_t sumE = 0.0f; 
  float rms = 0.0;
  float weight = 0.0;
  for (int i = 0; i < howmany && i < cellInfoAPion.size(); ++i) {  
    rms = lookupRMSmap(std::get<1>(cellInfoAPion[i]), std::get<3>(cellInfoAPion[i]));    
    weight = 1/(rms*rms);
    float offset = lookupMEANmap(std::get<1>(cellInfoAPion[i]), std::get<3>(cellInfoAPion[i]));
    sum += (std::get<0>(cellInfoAPion[i])-offset)*weight;
    sumE += weight;

    vec_frac.push_back(std::get<1>(cellInfoAPion[i])/std::get<2>(cellInfoAPion[i]));
  }
  if (howmany<cellInfoAPion.size()) {
    vec_ncell.push_back(howmany);
  }
  else {
    vec_ncell.push_back(cellInfoAPion.size());
  }

  float avg = -999.;
  avg = sum/sumE;
  return avg;
}

TPaveText* addTEXT(const std::string& text, double lowY, int color) {
  double lowX=0.17;
  TPaveText* lumi = new TPaveText(lowX, lowY + 0.06, lowX + 0.15, lowY + 0.16, "NDC");
  lumi->SetTextFont(61);
  lumi->SetTextSize(0.04);
  lumi->SetBorderSize(0);
  lumi->SetFillStyle(0);
  lumi->SetTextAlign(12);
  lumi->SetTextColor(color);
  lumi->AddText(text.c_str());
  return lumi;
}

int averageTimePlotter_merged(float fitMin, float fitMax, int nbin, float bmin, float bmax, std::string filename, int avgUptoN, float truthPionPtLow, float truthPionPtHigh){
  gROOT->SetBatch(kTRUE);
  ReadTxt(); // build RMS map one time
  
  std::string inputFileName = "../ntuples/" + filename + ".topo-cluster.001.new.root";
  TFile* inputFile = TFile::Open(inputFileName.c_str());
  TTree* tree = (TTree*)inputFile->Get("CellTree");
    
  Int_t eventNumber_before = -999.;
  std::vector<CellInfoTuple> cellInfoAPion; // cellTime and cellE
  Float_t cnt = 0.0;

  std::vector<Int_t> eventNumbers;
  std::vector<Float_t> cellTimeSums;

  Entry entry;
  tree->SetBranchAddress("eventNumber", &entry.eventNumber);
  tree->SetBranchAddress("cellTime", &entry.cellTime);
  tree->SetBranchAddress("cellEdep", &entry.cellEdep);
  tree->SetBranchAddress("cellE", &entry.cellE);
  tree->SetBranchAddress("cellEinPion", &entry.cellEinPion);
  tree->SetBranchAddress("cellEta", &entry.cellEta);
  tree->SetBranchAddress("cellCenterX", &entry.cellCenterX);
  tree->SetBranchAddress("cellCenterY", &entry.cellCenterY);  
  tree->SetBranchAddress("truthE", &entry.truthE);
  tree->SetBranchAddress("truthPt", &entry.truthPt);
  tree->SetBranchAddress("nCellsWithCuts", &entry.nCellsWithCuts);
  for (Long64_t i = 0; i < tree->GetEntries(); ++i) {
    tree->GetEntry(i);
    if (i==0) eventNumber_before = entry.eventNumber;
    if (i%5000000==0) std::cout << (float(i)/tree->GetEntries())*100 << " % " << std::endl;       
    float cellCenterR = TMath::Sqrt(entry.cellCenterX * entry.cellCenterX + entry.cellCenterY * entry.cellCenterY);
    
    if (entry.nCellsWithCuts<1) continue;
    if (entry.cellTime == 0) continue;
    if (entry.cellE < 0.1) continue;
    if (entry.cellEta > 0.8 || entry.cellEta < -0.8) continue; // barrel
    if (cellCenterR > 2100) continue; // LAr
    if (cellCenterR < 1500) continue; // LAr pre-sampler rejection
    if (cellCenterR > 1800) continue; 
    if (entry.truthPt < truthPionPtLow || entry.truthPt > truthPionPtHigh) continue; // discard low energy cells

    std::string layer = "NONE";
    if (cellCenterR < 1500) layer = "presampler";
    if (cellCenterR < 1600 && cellCenterR>=1500) layer = "layer1"; // LAr layer1
    if (cellCenterR < 1800 && cellCenterR>=1600) layer = "layer2"; // LAr layer2
    if (cellCenterR < 2100 && cellCenterR>=1800) layer = "layer3"; // LAr layer3
    if ((eventNumber_before == entry.eventNumber) || ((eventNumber_before != entry.eventNumber) && cellInfoAPion.size()==0)) {
      CellInfoTuple tupleInfoAPion(entry.cellTime, entry.cellE, entry.truthE, layer);
      cellInfoAPion.push_back(tupleInfoAPion);
      cnt += 1.0;
      eventNumber_before = entry.eventNumber;
    } else {
      // first, calculate average.
      Float_t maxValue = -999.;
      // only cells with at least more than 2 cells after selection
      if (avgUptoN<0) {
	maxValue = sortmaxValueInVector(cellInfoAPion, cellInfoAPion.size());
      } else {
	maxValue = sortmaxValueInVector(cellInfoAPion, avgUptoN);
      }
      cellTimeSums.push_back(maxValue);

      // clear up all containers for previous events
      eventNumber_before = entry.eventNumber;
      eventNumbers.push_back(entry.eventNumber); // not necessary - just have this for debugging usage
      cellInfoAPion.clear();

      CellInfoTuple tupleInfoAPion(entry.cellTime, entry.cellE, entry.truthE, layer);
      cellInfoAPion.push_back(tupleInfoAPion);
    }	
  }
  
  TH1D* h1 = new TH1D("h_cellTimeSum", "", nbin, bmin, bmax);
  drawHist(h1, cellTimeSums);
  h1->SetLineWidth(2);

  gStyle->SetFrameLineWidth(2);
  gStyle->SetLineWidth(1);

  TCanvas* canvas = new TCanvas("", "", 0, 0, 700, 700);
  canvas->cd();
  h1->SetMaximum(17500);    
  h1->Draw("HISTE");

  TPaveText* title = new TPaveText(0.20, 0.905, 0.80, 0.999, "NDC");
  title->SetFillColor(0);
  title->SetBorderSize(0);
  title->AddText("Average Cell Time in Single Pion");
  title->Draw("SAME");

  // fit
  TF1* fitFx = new TF1("fitFx", "gaus", fitMin, fitMax);
  h1->Fit(fitFx, "Q", "", fitMin, fitMax);
  fitFx->Draw("SAME");

  double gausMean = fitFx->GetParameter(1);
  double gausSigma = fitFx->GetParameter(2);

  TPaveText* mean = addTEXT(Form("Mean = %.2f ns", gausMean), 0.68, 2);
  TPaveText* sigma = addTEXT(Form("RMS = %.f ps", gausSigma * 1000), 0.63, 2);
  TPaveText* h_mean = addTEXT(Form("Mean = %.2f ns", h1->GetMean()), 0.55, 4);
  TPaveText* h_rms = addTEXT(Form("RMS = %.f ps", h1->GetRMS() * 1000), 0.5, 4);

  mean->Draw("SAME");
  sigma->Draw("SAME");
  h_mean->Draw("SAME");
  h_rms->Draw("SAME");
 

  std::string pngname = "plots/AverageCellTime.png";
  canvas->SaveAs(pngname.c_str());
  std::cout << "imgcat " << pngname << std::endl;

  delete h1;
  delete title;
  delete canvas;

  // h_frac will store histgram of cellE/truthE that used in the gaussian distribution
  drawHist(h_frac, vec_frac);
  TCanvas* c_frac = new TCanvas("", "", 0, 0, 700, 700);
  c_frac->cd();
  h_frac->SetMaximum(h_frac->GetMaximum()*100);
  h_frac->Draw("HISTE");

  TPaveText* t_frac = new TPaveText(0.20, 0.905, 0.80, 0.999, "NDC");
  t_frac->SetFillColor(0);
  t_frac->SetBorderSize(0);
  t_frac->AddText("Reco cell E / Truth Pion E");
  t_frac->Draw("SAME");

  TPaveText* integral_frac = new TPaveText(0.12, 0.8, 0.75, 0.85, "NDC");
  integral_frac->SetFillColor(0);
  integral_frac->SetBorderSize(0);

  float percentage =  h_frac->Integral(0,10)/h_frac->Integral();  
  std::string percentage_str = "Fraction below 10% = " + std::to_string(percentage);
  integral_frac->AddText(percentage_str.c_str());
  integral_frac->Draw("SAME");

  c_frac->SetLogy();
  c_frac->SaveAs("h_frac.png");

  delete h_frac;
  delete c_frac;

  // h_ncell will store histgram of #cells in the gaussian distribution from each pion
  drawHist(h_ncell, vec_ncell);
  TCanvas* c_ncell = new TCanvas("", "", 0, 0, 700, 700);
  c_ncell->cd();
  h_ncell->Draw("HISTE");

  TPaveText* integral_ncell = new TPaveText(0.12, 0.8, 0.75, 0.85, "NDC");
  integral_ncell->SetFillColor(0);
  integral_ncell->SetBorderSize(0);

  float singlecellcontribution = h_ncell->Integral(1,2)/h_ncell->Integral();
  std::string singlecellcontribution_str = "#cell==1 fraction = " + std::to_string(singlecellcontribution);
  integral_ncell->AddText(singlecellcontribution_str.c_str());
  integral_ncell->Draw("SAME");

  TPaveText* t_ncell = new TPaveText(0.20, 0.905, 0.80, 0.999, "NDC");
  t_ncell->SetFillColor(0);
  t_ncell->SetBorderSize(0);
  t_ncell->AddText("#cells from each pion");
  t_ncell->Draw("SAME");

  c_ncell->SaveAs("h_ncell.png");

  delete h_ncell;
  delete c_ncell;

  return 0;
}


