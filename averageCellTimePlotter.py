import os
import uproot
import numpy as np
import awkward as ak
import argparse
import vector, math, datetime
import printer
import CommandExecuter as ex
import ROOT 
from ROOT import gROOT
gROOT.SetBatch(ROOT.kTRUE)
np.set_printoptions(precision=3, suppress=True)

print ('awkward version :', ak.__version__) # 2.4.4
print ('uproot version :', uproot.__version__) # 5.0.12
parser = argparse.ArgumentParser(description = "Central script for running the differential analysis fits")
parser.add_argument('--PrinterMode', '-p', help='Just print command lines', action='store_true', default=False)
parser.add_argument('--VerboseMode', '-v', help='Verbose', action='store_true', default=False)
parser.add_argument('--InputDir', '-i', help='inputfile path', action='store', default='/lcrc/group/ATLAS/users/doyeong/5DCalorimeter/ntuples/')
parser.add_argument('--doFitRange', '-r', help='min,max', action='store', type=str, default = "-1.5,1.5")
parser.add_argument('--binning', '-b', help='binning,min,max', action='store', type=str, default = "50,-1.5,1.5")
parser.add_argument('--layer', '-l', help='variable to draw', action='store', type=str, default = "layer1")
parser.add_argument('--truthPt', help='truthPt range low, high', action='store', type=str, default = "1,20")
parser.add_argument('--histoMax', '-m', help='histogram Y-axis max', action='store', type=str, default = "-999")
parser.add_argument('--weightType', help='[RMS, cellE, Arithmetic]', action='store', type=str, default = "RMS")
parser.add_argument('--cellSignificance', help='cut on cellSignificance', action='store', type=str, default = "0,100000")
parser.add_argument('--cellE', help='cut on cellE', action='store', type=str, default = "0.5")
parser.add_argument('--nCluster', help='cut on nCluster', action='store', type=str, default = "-1")
parser.add_argument('--InputFile', '-f', help='cut on nCluster', action='store', type=str, default = "pizero.240117.1000files.1GeVbool.200GeVbool.highPtCellinLayer1or2.OverlapCellbool.skimmed.topo-cluster.001.new.root")
args = parser.parse_args()

binning = args.binning.split(",")
truthPtLow, truthPtHigh = (args.truthPt).split(',')[0], (args.truthPt).split(',')[1]
cellSignificanceLow, cellSignificanceHigh = (args.cellSignificance).split(',')[0], (args.cellSignificance).split(',')[1]
def addTEXT(text, lowY, color, textsize):
    lowX=0.17
    #lowY=0.7
    lumi  = ROOT.TPaveText(lowX, lowY+0.06, lowX+0.15, lowY+0.16, "NDC")
    if "pi" not in text: lumi.SetTextFont(61)        
    lumi.SetTextSize(textsize)
    lumi.SetBorderSize(    0 )
    lumi.SetFillStyle(     0 )
    lumi.SetTextAlign(    12 )
    lumi.SetTextColor( color )
    if 'cellSig' in text: lumi.AddText(text.replace("< 100000",""))
    else: lumi.AddText(text)
    '''
    if 'sigma' in text:
        latex = ROOT.TLatex()
        latex_formula = text.replace("< 100000","")
        latex.SetTextSize(textsize)
        latex.DrawLatex(0, 0, latex_formula)
        latex.SetTextAlign(11)
    else:
        lumi.AddText(text)
    '''
    return lumi

def doFit(h):
    fittype = "gaus"
    doFitRange = args.doFitRange.split(",")
    fitFx = h.Fit(fittype, "", "", float(doFitRange[0]), float(doFitRange[1]))
    fx = h.GetFunction(fittype)
    gausMean = fx.GetParameter(1)
    gausSigma = fx.GetParameter(2)
    titleFit = addTEXT("Gaus Fit", 0.68, 2, 0.04)
    mean = addTEXT("Mean = %.f ps"%(gausMean*1000), 0.63, 2, 0.04)
    sigma = addTEXT("RMS = %.f ps"%(gausSigma*1000), 0.58, 2, 0.04)
    return [fx, titleFit, mean, sigma]

def createHistogram(h_numpy, h_name, h_title, binning, layerInfo):
    # Create a TCanvas for drawing
    canvas = ROOT.TCanvas("", "", 700, 700)
    
    # Create a histogram for the averaged cell time distribution
    h = ROOT.TH1F(h_name, h_title, binning[0], binning[1], binning[2])
    for value in h_numpy: h.Fill(value)
    if 'std' not in h_name: fitComponents = doFit(h)
            
    # Draw the histogram
    h.GetYaxis().SetTitle("evt / bin")
    if 'cell_time_diff' in h_name:
        h.GetXaxis().SetTitle("Cell time differences [ns]")
    else:
        h.GetXaxis().SetTitle("Cell time [ns]")
    if float(args.histoMax)>0: 
        h.SetMaximum(args.histoMax)
    else:
        h.SetMaximum(h.GetMaximum()*1.3)
    h.Draw('HISTE')
    titleHist = addTEXT("Histogram", 0.50, 4, 0.04)
    h_mean = addTEXT("Mean = %.f ps"%(h.GetMean()*1000), 0.45, 4, 0.04)
    h_rms = addTEXT("RMS = %.f ps"%(h.GetRMS()*1000), 0.4, 4, 0.04)
    if 'std' not in h_name: titleHist.Draw("SAME")
    if 'std' not in h_name: h_mean.Draw("SAME")
    if 'std' not in h_name: h_rms.Draw("SAME")
    layerInfoText = addTEXT(layerInfo, 0.33, 1, 0.04)
    layerInfoText.Draw("SAME")
    #cellSignificanceCutText = addTEXT("%s < #varsigma_{cell} < %s"%(cellSignificanceLow, cellSignificanceHigh), 0.3, 1, 0.04)
    cellSignificanceCutText = addTEXT("%s < cellSig < %s"%(cellSignificanceLow, cellSignificanceHigh), 0.3, 1, 0.04)
    #cellSignificanceCutText.Draw("SAME")
    if args.nCluster != "-1":
        nClusterText = addTEXT("nCluster == %s"%(args.nCluster), 0.3, 1, 0.04)
        nClusterText.Draw("SAME")
    if 'std' not in h_name:
        for fitComponent in fitComponents: fitComponent.Draw('SAME') 
    pionErange = addTEXT('Truth Pion pT [%s, %s) GeV'%(truthPtLow, truthPtHigh), 0.75, 1, 0.04)
    pionErange.Draw("SAME")
    canvas.Update()
    pngname = 'plots/%s.png'%(h_name)
    canvas.SaveAs(pngname)
    printer.green('\nimgcat %s'%pngname)
    ex.imgCat(pngname)




def getCalib(isLayer1, isLayer2, cellE, cellCenterR):
    # /lcrc/group/ATLAS/users/doyeong/5DCalorimeter/timing-with-5d-calorimeter
    #  return ["TMath::Sqrt([0]*[0] + ([1]*[1]/x) + ([2]*[2]/x**2))", 3]
    if 'pizero' in args.InputFile:
        cRMS = [[0.058748, 0.001126, 0.023252], # LAr EM layer 1
                [0.045487, 0.092336, 0.107153],
                [0, 0, 0]] # LAr EM layer 2
    if 'piminus' in args.InputFile:
        cRMS = [[0.170658, 0.000320, 0.452637], # LAr EM layer 1
                [0.203315, 0.031128, 1.626447], # LAr EM layer 2
                [0.657360, 0.000332, 1.171347]] # LAr EM layer 2
    RMS1 = (np.sqrt(cRMS[0][0]**2 + ((cRMS[0][1]**2)/cellE) + ((cRMS[0][2]**2)/cellE**2)))*isLayer1 
    RMS2 = (np.sqrt(cRMS[1][0]**2 + ((cRMS[1][1]**2)/cellE) + ((cRMS[1][2]**2)/cellE**2)))*isLayer2
    RMS3 = (np.sqrt(cRMS[2][0]**2 + ((cRMS[2][1]**2)/cellE) + ((cRMS[2][2]**2)/cellE**2)))*isLayer2
    RMS = RMS1 + RMS2 + RMS3


    if args.VerboseMode is True:
        printer.info('check getRMS')
        getRMSdata = zip(cellCenterR, isLayer1, isLayer2, cellE, RMS1, RMS2, RMS)
        for i, x in enumerate(getRMSdata):
            print (x)
            if i > 30: break

    # return ["[0]*TMath::Exp(-[1]*x) + [2]*TMath::Exp(-[3]*x)", 4]
    if 'pizero' in args.InputFile:
        cOffset = [[0.009135, -0.070580, 0.024490, 2.527435], # LAr EM layer 1
                   [0.041562, 2.081075, 0.019199, 0.019314],  # LAr EM layer 2
                   [0, 0, 0, 0]]  # LAr EM layer 3
    if 'piminus' in args.InputFile:
        cOffset = [[0.140451, 2.051012, 0.611840, 23.695024], # LAr EM layer 1
                   [0.119308, 0.312582, 1.468094, 3.710413],  # LAr EM layer 2
                   [5.569136, 1.460981, -4.551469, 1.252174]] # LAr EM layer 3
    
    Offset1 = (cOffset[0][0]*np.exp(-cOffset[0][1]*cellE) + cOffset[0][2]*np.exp(-cOffset[0][3]*cellE))*isLayer1 
    Offset2 = (cOffset[1][0]*np.exp(-cOffset[1][1]*cellE) + cOffset[1][2]*np.exp(-cOffset[1][3]*cellE))*isLayer2 
    Offset3 = (cOffset[2][0]*np.exp(-cOffset[2][1]*cellE) + cOffset[2][2]*np.exp(-cOffset[2][3]*cellE))*isLayer2 
    Offset = Offset1 + Offset2 + Offset3

    if args.VerboseMode is True:
        printer.info('check t0 offset')
        printer.blue('cellCenterR, isLayer1, isLayer2, cellE, Offset1, Offset2, Offset')
        getOffsetdata = zip(cellCenterR, isLayer1, isLayer2, cellE, Offset1, Offset2, Offset)
        for i, x in enumerate(getOffsetdata):
            print (x)
            if i > 30: break

    return RMS, Offset


def groupByEvent(InputPath, layerSelection):
    printer.whiteBlueBold(f'    {layerSelection}    ')
    fname = InputPath
    namu = uproot.open(fname)['CellTree']
    branches = namu.arrays()
    masks = [
        (branches['cellEta'] > -0.8) & (branches['cellEta'] < 0.8),
        branches['cellTime'] != 0,
        branches['isOverlapCell'] == False,
        branches['nCellsWithCuts'] > 0,
        branches['cellE'] > float(args.cellE),                
        #branches['nCluster'] == 2,                
        (branches['truthPt'] > float(truthPtLow)) & (branches['truthPt'] < float(truthPtHigh)),        
    ]
    if 'pizero' in args.InputFile: masks.append((branches['cellSignificance'] > float(cellSignificanceLow)) & (branches['cellSignificance'] < float(cellSignificanceHigh)))
    if 'piminus' in args.InputFile: masks.append(branches['cellEdep'] > 0.001)
    if int(args.nCluster) > 0: masks.append(branches['nCluster'] == int(args.nCluster))
    # combine all maskes
    mask = np.all(np.column_stack(masks), axis=1)
    # branches
    cellTime = branches['cellTime'][mask]
    cellE = branches['cellE'][mask]
    eventNumber = branches['eventNumber'][mask]
    cellCenterX = branches['cellCenterX'][mask]
    cellCenterY = branches['cellCenterY'][mask]
    #cellCenterR = np.sqrt(ak.to_numpy(cellCenterX)**2 + ak.to_numpy(cellCenterY)**2)
    cellCenterR = np.sqrt(cellCenterX**2 + cellCenterY**2)
    isLayer1 = (cellCenterR > 1500) & (cellCenterR < 1600)
    isLayer2 = (cellCenterR > 1600) & (cellCenterR < 1800)
    isLayer3 = (cellCenterR > 1800) & (cellCenterR < 2100)
    RMS, Offset = getCalib(isLayer1, isLayer2, cellE, cellCenterR)

    # Due to layer requirements, 1./RMS can be 1/0    
    layerMasks = []
    if 'LArEM1' in layerSelection: layerMasks.append(isLayer1)
    if 'LArEM2' in layerSelection: layerMasks.append(isLayer2)
    if 'LArEM3' in layerSelection: layerMasks.append(isLayer3)
    layerMask = np.any(np.column_stack(layerMasks), axis=1)
    cellTimeCalibed = cellTime[layerMask]-Offset[layerMask]
    if args.weightType=='RMS': weight = 1./RMS[layerMask]**2 # before dividing
    elif args.weightType=='Arithmetic': weight = np.ones_like(cellTimeCalibed)
    elif args.weightType=='cellE': weight = cellE[layerMask] # before dividing

    # Create a structured array with 'cellTime' and 'eventNumber'
    structured_data = np.array(list(zip(eventNumber[layerMask], cellTimeCalibed, weight, cellCenterR[layerMask], cellE[layerMask])), 
                               dtype=[('eventNumber', 'i4'), ('cellTime', 'f4'), ('weight', 'f4'), ('cellCenterR', 'f4'), ('cellE', 'f4')])

    createHistogram(cellTimeCalibed,
                    "Unweighted_cell_time_%s"%(layerSelection.replace('&', 'and')), "%s Unweighted Cell Time"%(args.weightType),
                    [int(binning[0]), float(binning[1]), float(binning[2])], layerSelection.replace('&', 'and'))
    createHistogram(cellTimeCalibed*weight,
                    "Weighted_cell_time_%s"%(layerSelection.replace('&', 'and')), "%s Weighted Cell Time"%(args.weightType),
                    [int(binning[0]), float(binning[1]), float(binning[2])], layerSelection.replace('&', 'and'))

    if args.VerboseMode is True: 
        printer.info('# total cells: %.f'%(len(structured_data)))
        printer.blue('(eventNumber, cellTimeCalibed, weight. cellCenterR)')
        for i, x in enumerate(structured_data):
            if i<100: print(x)
    # Use numpy to group by 'eventNumber' and calculate the mean
    unique_event_numbers = np.unique(structured_data['eventNumber'])

    # Calculate the weighted mean for each unique eventNumber
    grouped_weighted_avgs = np.array([
        (
            number,
            len(structured_data[structured_data['eventNumber'] == number]),
            np.average(
                structured_data[structured_data['eventNumber'] == number]['cellTime'],
                weights=structured_data[structured_data['eventNumber'] == number]['weight']
            ),
            np.std(
                structured_data[structured_data['eventNumber'] == number]['cellTime']
            )
        ) for number in unique_event_numbers
    ], dtype=[('eventNumber', 'i4'), ('element_count', 'i4'), ('weighted_avg_cellTime', 'f4'), ('weighted_avg_cellTime_std', 'f4')])

    
    for j, number in enumerate(unique_event_numbers):
        print ('\n---------------------------------------------------------------------')
        print ('event number             : %.f'%number)
        print ('weighted_avg_cellTime    : %.4f ns'%grouped_weighted_avgs['weighted_avg_cellTime'][j])
        print ('weighted_avg_cellTime_std: %.4f ns'%grouped_weighted_avgs['weighted_avg_cellTime_std'][j])
        print ('=====================================================================')        
        weight_inSinclePion = structured_data[structured_data['eventNumber'] == number]['weight']
        cellE_inSinclePion = structured_data[structured_data['eventNumber'] == number]['cellE']
        cellCenterR_inSinclePion = structured_data[structured_data['eventNumber'] == number]['cellCenterR']
        cellTime_inSinclePion = structured_data[structured_data['eventNumber'] == number]['cellTime']
        for i, e in enumerate(cellE_inSinclePion):
            print('%.3f GeV\t %.3f ns \t weight: %.2f \t %.2f mm'%(e, cellTime_inSinclePion[i], weight_inSinclePion[i], cellCenterR_inSinclePion[i]))
        print ('---------------------------------------------------------------------')

    if args.VerboseMode is True: 
        printer.info('# total pions: %.f'%(len(grouped_weighted_avgs)))

        printer.info('grouped_weighted_avgs')
        printer.blue('(eventNumber, #cells, weighted_avg_cellTime)')
        for i, x in enumerate(grouped_weighted_avgs):
            if i<30: print (x)

    return grouped_weighted_avgs

def findArrayDiffExclusive(grouped_weighted_avgs_1, grouped_weighted_avgs_2):
    # Find exclusive event numbers in the first array
    exclusive_event_numbers_1 = np.setdiff1d(grouped_weighted_avgs_1['eventNumber'], grouped_weighted_avgs_2['eventNumber'])
    # Find exclusive event numbers in the second array
    exclusive_event_numbers_2 = np.setdiff1d(grouped_weighted_avgs_2['eventNumber'], grouped_weighted_avgs_1['eventNumber'])
    # Filter the arrays to include only exclusive event numbers
    exclusive_entries_1 = grouped_weighted_avgs_1[np.isin(grouped_weighted_avgs_1['eventNumber'], exclusive_event_numbers_1)]
    exclusive_entries_2 = grouped_weighted_avgs_2[np.isin(grouped_weighted_avgs_2['eventNumber'], exclusive_event_numbers_2)]

    return exclusive_entries_1, exclusive_entries_2


def findArrayDiff(grouped_weighted_avgs_1, grouped_weighted_avgs_2):
    # Find common event numbers between the two arrays
    common_event_numbers = np.intersect1d(grouped_weighted_avgs_1['eventNumber'], grouped_weighted_avgs_2['eventNumber'])

    # Filter the arrays to include only common event numbers
    common_entries1 = grouped_weighted_avgs_1[np.isin(grouped_weighted_avgs_1['eventNumber'], common_event_numbers)]
    common_entries2 = grouped_weighted_avgs_2[np.isin(grouped_weighted_avgs_2['eventNumber'], common_event_numbers)]
    difference = common_entries1['weighted_avg_cellTime'] - common_entries2['weighted_avg_cellTime']
    return difference, common_entries1, common_entries2

def findArrayDiff3Arr(grouped_weighted_avgs_1, grouped_weighted_avgs_2, grouped_weighted_avgs_3):
    # Find common event numbers among the three arrays
    common_event_numbers = np.intersect1d(
        np.intersect1d(
            grouped_weighted_avgs_1['eventNumber'], 
            grouped_weighted_avgs_2['eventNumber']),
        grouped_weighted_avgs_3['eventNumber']
    )

    # Filter the arrays to include only common event numbers
    common_entries1 = grouped_weighted_avgs_1[np.isin(grouped_weighted_avgs_1['eventNumber'], common_event_numbers)]
    common_entries2 = grouped_weighted_avgs_2[np.isin(grouped_weighted_avgs_2['eventNumber'], common_event_numbers)]
    common_entries3 = grouped_weighted_avgs_3[np.isin(grouped_weighted_avgs_3['eventNumber'], common_event_numbers)]
    #difference = common_entries1['weighted_avg_cellTime'] - common_entries2['weighted_avg_cellTime']
    return common_entries1, common_entries2, common_entries3

def main():
    printer.whiteBlackBold("     START     ")
    start = datetime.datetime.now()
    #InputPath = args.InputDir+'pizero.240117.1000files.1GeVbool.200GeVbool.highPtCellinLayer1or2.OverlapCellbool.skimmed.topo-cluster.001.new.root'
    InputPath = args.InputDir+'/'+args.InputFile
    #InputPath = args.InputDir+'pizero.1GeVbool.highPtCellinLayer1or2.OverlapCellbool.skimmed.topo-cluster.001.new.root'
    printer.gray("Inputfile: %s"%(InputPath))
    ex.isFileExist(InputPath)

    
    grouped_weighted_avgs_1 = groupByEvent(InputPath, 'LArEM1')
    grouped_weighted_avgs_2 = groupByEvent(InputPath, 'LArEM2')
    grouped_weighted_avgs_3 = groupByEvent(InputPath, 'LArEM1&LArEM2')
    if 'piminus' in args.InputFile:
        #grouped_weighted_avgs_LArEM3 = groupByEvent(InputPath, 'LArEM3')
        grouped_weighted_avgs_LArEM123 = groupByEvent(InputPath, 'LArEM1&LArEM2&LArEM3')

    createHistogram(grouped_weighted_avgs_1['weighted_avg_cellTime'],
                    "averaged_cell_time_LArEM1", "%s Averaged Cell Time per pion"%(args.weightType),
                    [int(binning[0]), float(binning[1]), float(binning[2])], 'LArEM1')
    createHistogram(grouped_weighted_avgs_2['weighted_avg_cellTime'],
                    "averaged_cell_time_LArEM2", "%s Averaged Cell Time per pion"%(args.weightType),
                    [int(binning[0]), float(binning[1]), float(binning[2])], 'LArEM2')
    createHistogram(grouped_weighted_avgs_3['weighted_avg_cellTime'],
                    "averaged_cell_time_LArEM1and2", "%s Averaged Cell Time per pion"%(args.weightType),
                    [int(binning[0]), float(binning[1]), float(binning[2])], 'LArEM1&LArEM2')
    if 'piminus' in args.InputFile:
        #createHistogram(grouped_weighted_avgs_LArEM3['weighted_avg_cellTime'],
        #                "averaged_cell_time_LArEM3", "%s Averaged Cell Time"%(args.weightType),
        #                [int(binning[0]), float(binning[1]), float(binning[2])], 'LArEM3')
        createHistogram(grouped_weighted_avgs_LArEM123['weighted_avg_cellTime'],
                        "averaged_cell_time_LArEM1and2and3", "%s Averaged Cell Time"%(args.weightType),
                        [int(binning[0]), float(binning[1]), float(binning[2])], 'LArEM1&LArEM2&LArEM3')

    stdmin, stdmax = 0, 0.5
    # std of calibrated cellTime
    createHistogram(grouped_weighted_avgs_1['weighted_avg_cellTime_std'],
                    "averaged_cell_time_std_LArEM1", "%s Averaged Cell Time Std. within a pion"%(args.weightType),
                    [int(binning[0]), stdmin, stdmax], 'LArEM1')
    createHistogram(grouped_weighted_avgs_2['weighted_avg_cellTime_std'],
                    "averaged_cell_time_std_LArEM2", "%s Averaged Cell Time Std. within a pion"%(args.weightType),
                    [int(binning[0]), stdmin, stdmax], 'LArEM2')
    createHistogram(grouped_weighted_avgs_3['weighted_avg_cellTime_std'],
                    "averaged_cell_time_std_LArEM1and2", "%s Averaged Cell Time Std. within a pion"%(args.weightType),
                    [int(binning[0]), stdmin, stdmax], 'LArEM1&LArEM2')
    if 'piminus' in args.InputFile:
            #createHistogram(grouped_weighted_avgs_LArEM3['weighted_avg_cellTime_std'],
            #                "averaged_cell_time_std_LArEM3", "%s Averaged Cell Time Std."%(args.weightType),
            #                [int(binning[0]), 0, 3], 'LArEM3')
            createHistogram(grouped_weighted_avgs_LArEM123['weighted_avg_cellTime_std'],
                            "averaged_cell_time_std_LArEM1and2and3", "%s Averaged Cell Time Std."%(args.weightType),
                            [int(binning[0]), 0, 3], 'LArEM1&LArEM2&LArEM3')
    '''
    # Calculate the difference in weighted_avg_cellTime
    difference12 = findArrayDiff(grouped_weighted_avgs_1, grouped_weighted_avgs_2)[0]
    createHistogram(difference12,
                    "cell_time_diff_LArEM1_minus_LArEM2", "%s Averaged Cell Time Diff"%(args.weightType),
                    [int(binning[0]), -0.3, 0.3], 'LArEM1-LArEM2')

    difference32 = findArrayDiff(grouped_weighted_avgs_3, grouped_weighted_avgs_2)[0]
    createHistogram(difference32,
                    "cell_time_diff_LArEM1andLArEM2_minus_LArEM2", "%s Averaged Cell Time Diff"%(args.weightType),
                    [int(binning[0]), -0.3, 0.3], 'LArEM(1+2)-LArEM2')

    print ('difference32')
    print (difference32)
    print (np.count_nonzero(difference32 == 0, axis=0))

    difference31 = findArrayDiff(grouped_weighted_avgs_3, grouped_weighted_avgs_1)[0]
    createHistogram(difference31,
                    "cell_time_diff_LArEM1andLArEM2_minus_LArEM1", "%s Averaged Cell Time Diff"%(args.weightType),
                    [int(binning[0]), -0.3, 0.3], 'LArEM(1+2)-LArEM1')

    # extract exclusive array
    exclusive_entries1, exclusive_entries2 = findArrayDiffExclusive(grouped_weighted_avgs_1, grouped_weighted_avgs_2)
    createHistogram(exclusive_entries1['weighted_avg_cellTime'],
                    "averaged_cell_time_LArEM1_exclusive", "%s Averaged Cell Time (exclusive)"%(args.weightType),
                    [int(binning[0]), float(binning[1]), float(binning[2])], 'LArEM1')
    createHistogram(exclusive_entries2['weighted_avg_cellTime'],
                    "averaged_cell_time_LArEM2_exclusive", "%s Averaged Cell Time (exclusive)"%(args.weightType),
                    [int(binning[0]), float(binning[1]), float(binning[2])], 'LArEM2')

    # extract common array between all three cases
    common_entries1, common_entries2, common_entries3 = findArrayDiff3Arr(grouped_weighted_avgs_1, grouped_weighted_avgs_2, grouped_weighted_avgs_3)
    
    createHistogram(common_entries1['weighted_avg_cellTime'],
                    "averaged_cell_time_LArEM1_common", "%s Averaged Cell Time (common)"%(args.weightType),
                    [int(binning[0]), float(binning[1]), float(binning[2])], 'LArEM1')
    createHistogram(common_entries2['weighted_avg_cellTime'],
                    "averaged_cell_time_LArEM2_common", "%s Averaged Cell Time (common)"%(args.weightType),
                    [int(binning[0]), float(binning[1]), float(binning[2])], 'LArEM2')
    createHistogram(common_entries3['weighted_avg_cellTime'],
                    "averaged_cell_time_LArEM1and2_common", "%s Averaged Cell Time (common)"%(args.weightType),
                    [int(binning[0]), float(binning[1]), float(binning[2])], 'LArEM(1+2)')

    # Calculate the difference in weighted_avg_cellTime
    difference12 = common_entries1['weighted_avg_cellTime'] - common_entries2['weighted_avg_cellTime']
    createHistogram(difference12,
                    "cell_time_diff_LArEM1_minus_LArEM2_common", "%s Averaged Cell Time Diff (common)"%(args.weightType),
                    [int(binning[0]), -0.3, 0.3], 'LArEM1-LArEM2')

    difference32 = common_entries3['weighted_avg_cellTime'] - common_entries2['weighted_avg_cellTime']
    createHistogram(difference32,
                    "cell_time_diff_LArEM1and2_minus_LArEM2_common", "%s Averaged Cell Time Diff (common)"%(args.weightType),
                    [int(binning[0]), -0.3, 0.3], 'LArEM(1+2)-LArEM2')

    difference31 = common_entries3['weighted_avg_cellTime'] - common_entries1['weighted_avg_cellTime']
    createHistogram(difference31,
                    "cell_time_diff_LArEM1and2_minus_LArEM1_common", "%s Averaged Cell Time Diff (common)"%(args.weightType),
                    [int(binning[0]), -0.3, 0.3], 'LArEM(1+2)-LArEM1')

    '''
    end = datetime.datetime.now()
    print ("running time: %s"%(str(end-start).split(".")[0]))

if __name__ == "__main__":
        main()

