# 5D Calorimeter



## Matching CellTree and ClusterTree

```
root -L AddRunNumberToCellTree.C
```

## Plot cell level 
This plotter work just like interactive mode Draw with preset plot style
```
python CellTreePlotter.py -v "cellTime" -b "100,-3,3" -a "cellTime!=0" -s "pizero.eventNumberAdded_1008" -f -r="-3,3"
```
-v variables
-b binning nbins, min, max
-a cut
-s sample
-f do Gaus fit
-r fitting range

## Ntuple location
```
/afs/cern.ch/work/d/doyeong/public/for5D
```

